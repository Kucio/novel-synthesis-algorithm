#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <numeric>
#include <array>

//classes and structs
struct Element
{
	Element() = default;
	Element(unsigned int one, unsigned int two, unsigned int three) : values{one, two, three} {};
	static const unsigned int elemSize = 3;
	bool ifVisited = false;
	std::vector<unsigned int> values = decltype(values)(elemSize); //define size of vector
};

//global variables
unsigned int tableSize = 8;
const std::vector<Element> truthTable{
	Element(0, 0, 0),
	Element(0, 0, 1),
	Element(0, 1, 0),
	Element(0, 1, 1),
	Element(1, 0, 0),
	Element(1, 0, 1),
	Element(1, 1, 0),
	Element(1, 1, 1)};
std::string finalCircut = "";

//functions
void load(std::string fileName, std::vector<Element> &in)
{
	std::vector<unsigned int> tempValues;
	unsigned int temp;
	std::fstream file;
	file.open(fileName, std::ios::in);
	while (file >> temp)
	{
		tempValues.push_back(temp);
	}
	auto counter = 0;
	for (auto &e : in)
	{
		for (auto &val : e.values)
		{
			val = tempValues[counter];
			counter++;
		}
	}
	if (counter != 24)
	{
		std::cout << "Bad amount of numbers given in input file" << std::endl;
		abort();
	}
	file.close();
}

bool check(std::vector<Element> &in)
{
	for (unsigned int col = 0; col < Element::elemSize; col++)
	{
		for (unsigned int row = 0; row < in.size(); row++)
		{
			if (in[row].values[col] != truthTable[row].values[col])
			{
				return true;
			}
		}
	}
	return false;
}

bool validate(std::vector<Element> &in)
{
	auto counter = 0;
	std::vector<std::string> patterns = {"000", "001", "010", "011", "100", "101", "110", "111"};
	for (unsigned int row = 0; row < in.size(); row++)
	{
		std::string s;
		for (unsigned int col = 0; col < Element::elemSize; col++)
		{
			s.append(std::to_string(in[row].values[col]));
		}
		for (unsigned int i = 0; i < patterns.size(); i++)
		{
			if (patterns[i] == s)
			{
				counter++;
				patterns.erase(patterns.begin() + i);
				break;
			}
		}
	}
	if (counter == 8)
	{
		return true;
	}
	else
	{
		std::cout << "Bad input!!!! "
				  << "Proper values found: " << counter;
		abort();
	}
}

void printResults(unsigned int col, unsigned int row, unsigned int change, std::vector<Element> &in)
{
	std::string s("T: ");
	std::cout << "Move: " << std::endl;
	std::cout << col << " " << row << " " << change << std::endl;
	std::cout << "\t";
	for (auto e : in[row].values)
	{
		std::cout << e << " ";
	}
	std::cout << "\t";
	for (auto e : in[change].values)
	{
		std::cout << e << " ";
	}
	std::cout << std::endl;
	if (col == 0)
	{
		if (in[row].values[col + 2] == 0)
			s.append("a' ");
		else
			s.append("a ");
		if (in[row].values[col + 1] == 0)
			s.append("b' ");
		else
			s.append("b ");
		s.append("c");
	}
	if (col == 1)
	{
		if (in[row].values[col + 1] == 0)
			s.append("a' ");
		else
			s.append("a ");
		if (in[row].values[col - 1] == 0)
			s.append("c' ");
		else
			s.append("c ");
		s.append("b");
	}
	if (col == 2)
	{
		if (in[row].values[col - 1] == 0)
			s.append("b' ");
		else
			s.append("b ");
		if (in[row].values[col - 2] == 0)
			s.append("c' ");
		else
			s.append("c ");
		s.append("a");
	}
	std::cout << s << std::endl;
	finalCircut.append(s);
	finalCircut.append("\n");
}

void novelSynthesisBasedAlgorithm(std::vector<Element> &in)
{
	std::cout << "Moving scheme: " << std::endl;
	std::cout << "[column] [row] [row to switch]" << std::endl;
	auto counter = 0;
	while (check(in))
	{
		for (unsigned int col = 0; col < Element::elemSize; col++)
		{
			for (unsigned int row = 0; row < in.size(); row++)
			{
				if (in[row].values[col] != truthTable[row].values[col])
				{
					//start from next row
					for (unsigned int i = row + 1; i < in.size(); i++)
					{
						//check if found value is ok for replacement
						//and is not already on the right spot
						if (in[i].values[col] == truthTable[row].values[col] && in[i].ifVisited == false && in[row].ifVisited == false && in[i].values[col] != in[row].values[col])
						{
							//check if more than 2 fields are not modified at once
							//before swap on last element in last column
							//check if not continuing to the element out of scope
							if ((col == 0 && in[i].values[col + 1] != in[row].values[col + 1]) ||
								(col == 0 && in[i].values[col + 2] != in[row].values[col + 2]))
							{
								if (row == tableSize - 1)
									break;
								continue;
							}
							if ((col == 1 && in[i].values[col - 1] != in[row].values[col - 1]) ||
								(col == 1 && in[i].values[col + 1] != in[row].values[col + 1]))
							{
								if (row == tableSize - 1)
									break;
								continue;
							}
							if ((col == 2 && in[i].values[col - 2] != in[row].values[col - 2]) ||
								(col == 2 && in[i].values[col - 1] != in[row].values[col - 1]))
							{
								if (row == tableSize - 1)
									break;
								continue;
							}
							else
							{
								printResults(col, row, i, in);
								std::swap(in[i], in[row]);
								counter++;
								in[i].ifVisited = true;
								in[row].ifVisited = true;
							}
						}
					}
				}
			}
			for (unsigned int row = 0; row < in.size(); row++)
				in[row].ifVisited = false;
		}
	}
	std::cout << std::endl
			  << "Moves counter: " << counter << std::endl;
}

void print(const std::vector<Element> &in)
{
	for (auto &e : in)
	{
		for (auto &e2 : e.values)
		{
			std::cout << e2 << " ";
		}
		std::cout << std::endl;
	}
}
int main(int argc, const char *argv[])
{
	(void)argc;
	std::vector<Element> input{tableSize};
	load(argv[1], input);
	validate(input);
	std::cout << std::endl
			  << "Input values: " << std::endl;
	std::cout << "c b a" << std::endl;
	print(input);
	std::cout << std::endl;
	novelSynthesisBasedAlgorithm(input);
	std::cout << "Output values: " << std::endl;
	std::cout << "c b a" << std::endl;
	print(input);
	std::cout << std::endl;
	std::cout << "Final Circut: " << std::endl;
	std::cout << "Circut gates are printed from bottom, last printed gate is first input gate for generated circut" << std::endl;
	std::cout << finalCircut;
}
